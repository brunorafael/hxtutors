var gulp = require('gulp');
var sass = require('gulp-sass');

//task for the sass
gulp.task('sass', function(){
   gulp.src('public/sass/**/*.sass')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(gulp.dest('public/css'))
})

//task for watch
gulp.task('watch', () =>
    gulp.watch('public/sass/**/*.sass', ['sass'])
)

//task for default gulp
gulp.task('default', ['sass', 'watch'])